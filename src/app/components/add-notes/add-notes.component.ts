import { Component, OnInit } from '@angular/core';
import { Notes } from '../../models/Notes';
import { NoteServiceService } from '../../services/note-service.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-notes',
  templateUrl: './add-notes.component.html',
  styleUrls: ['./add-notes.component.css']
})
export class AddNotesComponent implements OnInit {
  noteTitle: String;
  noteContent: String;
  noteCategory: String;
  submitted:boolean;

  constructor(private noteService: NoteServiceService) { }

  ngOnInit(): void {
  }

  // Methods
  addNote() {
    var note = new Notes();
    note.title = this.noteTitle;
    note.content = this.noteContent;
    note.category = this.noteCategory;
    this.noteService.addNote(note);
  }

  submitForm(form: NgForm) {
    if(form.valid) {
      this.submitted = true;
    }
  }

}
