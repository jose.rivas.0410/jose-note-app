import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Notes } from '../../models/Notes';
import { NoteServiceService } from '../../services/note-service.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {
  notes:Notes[] = [];

  someHeroes:Notes[];

  isEdit: boolean = false;

  selectedNote:Notes;

  form;

  constructor(private noteService: NoteServiceService) { }

  ngOnInit(): void {
    this.someHeroes = [{
      id: 0,
    title: 'Filler',
    content: 'lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem lorem ',
    category: 'Filler-text'
    }];

    this.getMyNotes();
    setInterval(() => { this.getMyNotes() }, 1);
  }

  // Methods
  getMyNotes() {
    this.notes = this.noteService.getNotes();
  };

  deleteNote(id:number) {
    this.noteService.deleteNote(id);
  };
}