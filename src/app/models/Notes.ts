export class Notes {
    id: Number;
    title: String = '';
    content: String = '';
    category: String = '';

    constructor(values: Object = {}) {
        Object.assign(this, values);
    }
}