import { Injectable } from '@angular/core';
import { Notes } from '../models/Notes';

@Injectable({
  providedIn: 'root'
})
export class NoteServiceService {
  lastId:number = 0;
  notes: Notes[] = [];

  constructor() { }

  // Methods
  getNotes():Notes[]{
    return this.notes;
  }

  addNote(note:Notes):NoteServiceService {
    if(!note.id){
      note.id = ++this.lastId;
    }
    this.notes.push(note);
    return this;
  }

  deleteNote(id:number):NoteServiceService {
    this.notes = this.notes.filter(note => note.id !== id);
    return this;
  }

  // editNote(id:number):NoteServiceService {
  //   this.notes = this.notes.filter(note => note.id !== id)
  //   return this;
  // }
}
